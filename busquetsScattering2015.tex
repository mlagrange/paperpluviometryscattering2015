% Template for ICASSP-2010 paper; to be used with:
%          mlspconf.sty  - ICASSP/ICIP LaTeX style file adapted for MLSP, and
%          IEEEbib.bst - IEEE bibliography style file.
% --------------------------------------------------------------------------
\documentclass{article}
\usepackage{amsmath,graphicx,spconf,paralist,multicol,tabularx,color}
	


\graphicspath{{figures/}}

\newcommand{\mathieu}[1]{\textcolor{red}{Mathieu : #1}}
\newcommand{\gerard}[1]{\textcolor{blue}{Gerard : #1}}


% Example definitions.
% --------------------
\def\x{{\mathbf x}}
\def\L{{\cal L}}

% Title.
% ------
\title{Classification of rainfall radar images \\ using the scattering transform}
%

%
% For example:
% ------------
%\address{School\\
%	Department\\
%	Address}
%
% Two addresses (uncomment and modify for two-address case).
% ----------------------------------------------------------
\twoauthors
  {Gerard Busquets Garcia, Mathieu Lagrange }
	{   IRCCYN 
	CNRS, Ecole Centrale de Nantes\\
	1, rue de la No\'e, 44000 Nantes \\
	(\texttt{mathieu.lagrange@cnrs.fr})}
  {Isabelle Emmanuel, Herv\'e Andrieu}
	{PRES L’UNAM, IFSTTAR,  \\ Laboratoire Eau et Environnement, \\ and IRSTV FR CNRS 2488, Bouguenais}

\renewcommand{\baselinestretch}{1.02}

\begin{document}
%\ninept
%

\maketitle
%
\begin{abstract}
The main objective of this paper is to
classify rainfall radar images by using the scattering transform, which 
gives us a translation invariant representation of the images and preserves 
high-frequency information useful to encode important morphological aspects of the meteorological phenomena under study. 
%The classification is performed with the algorithm k-nearest neighbours.
%A total of 1239 images, contained in 34 rain periods have been previously 
%classified by visual analysis into 4 classes: Light rain,shower, organised 
%storm and unorganised storm.
To demonstrate the usefulness of the approach, a classification framework is considered, where the images are to be classified into 4 morphological classes: light rain, shower, unorganised storm and organised storm. Experiments show that the benefits of the scattering are threefold: 1) it provides complementary information with respect to more traditional features computed over the distribution of the rainfall intensities, 2) it provides strong invariance to deformations, 3) second order coefficients of the scattering transform nicely encodes spatial distribution of rain intensity.
\end{abstract}

\begin{keywords}
Classification, scattering, image radar processing
\end{keywords}

\section{Introduction}
\label{sec:intro}

Detection of the type of rainfall is interesting for urban hydrology. Depending on the type of rainfall, the water will spread very differently into the urban surfaces thus leading to different management options. From light rain to storms, the morphology of the meteorological phenomena can be very different. This variability occurs also within each class of rainfall.

Radar rainfall images are therefore a very challenging case-study for feature design. Even though spatial information is important to model as it conveys the structure of the meteorological phenomenon under interest, it has to be taken into account that invariance to translation, rotation and small deformations are also desirable. Indeed, 
each meteorological phenomenon will go through the radar area in different locations and angles, and its shape will vary while preserving global properties that can easily be recognized by a trained human. Those properties are non trivially expressed by the actual images. Some studies modeled rainfall fields as fractals \cite{Lovejoy1985mandelbrot, Lovejoy1985model} or with spatial variograms \cite{Emmanuel2012162}, with, to the best of our knowledge, no application to feature design for rain type classification. 

We propose in this paper to tackle this issue by considering the scattering framework proposed by Mallat \cite{CPA:CPA21413}. This framework features most of the invariance constraints expressed above and has shown high performance in texture classification \cite{6522407}, thanks to its ability to model structured shapes by incorporating high order moments which can discriminate non-Gaussian properties.

The remaining of the paper is organized as follows. Section \ref{sec:problem} presents some background information on the case study with the main characteristics of the 4 types of rainfalls as well as previously proposed features that consider the distribution of the rain intensity. Section \ref{sec:scattering} and \ref{sec:implantation} explain how the 
scattering transform works and how this new approach can  help us 
to classify those images.
Finally, section \ref{sec:experiments} shows the performance
of the scattering transform and its complementarity with intensity distribution statistics.

\begin{figure*}[t]
\centering
\centerline{\includegraphics[width=\linewidth]{figures/Rainfalltypes}}
\caption{The 4 types of rainfall.}
\label{fig:Light rain}
\end{figure*}


\section{Background}
\label{sec:problem}

%Figure \ref{fig:Light rain} and figure \ref{fig:Shower} show also the difference
%between different periods and how this case study could be extrapoled into 
%many other subjects. Figure \ref{fig:Light rain} shows an image similar to a texture
%which has a high number of low-intensity pixels; figure \ref{fig:Shower} is more 
%similar at what could be a sample seen with a microscope. 

The data considered in this study have been analyzed in \cite{Emmanuel2012162} and highlighted at the mesoscale (ranging horizontally from around 5 kilometers to several hundred kilometers), three categories of meteorological situations: warm sectors, front and tail end of low pressure systems. Those meteorological situations give
four different types of rainfall fields:
\begin{itemize}
\item Light rain (linked to warm sectors).
\item Showers (linked to tail end of low pressure systems).
\item Storms less organized (linked to fronts).
\item Storms organized in rain bands (linked to fronts).
\end{itemize}

\subsection{Typology of rainfall}

In order to discriminate between those phenomena, some morphological aspects can be underlined:
\begin{itemize}
\item[Light rain:]
 has few number of no-rain areas. However, in these rainy areas, the intensity is generally low compared with the other rain types.
\item[Shower:]
can be considered as the opposite case of the light rain. The rain takes up little space in the area, but it has generally higher variance, which also turns into higher intensity.
\item[Unorganised storms:]
 storms that share some characteristics with showers. They are characterized by spread areas with both high and low intensities, that are somewhat disorganized.
\item[Organised storms:]
 storms that are characterized by large bands, which have both high and low intensities with higher rain intensity in the middle of the band.
\end{itemize}




The image database is formed by rain periods. Each rain period contains a group of images
 which show the rain intensity every 5 minutes in the incidence zone of the radar \cite{Berne2004166}. The images display an area of 40X50 km and every pixel gives the data of the rain 
intensity for 250X250m. 0 indicates a lack of rain. The higher the intensity, the higher the number is. In total, there are 34 rain periods for a total of 1239 images \cite{Emmanuel2012162, Emmanuel201220}.


The images have been received from the Treillieres radar, a C-band radar from the 
ARAMIS network located 10 km north from Nantes.
A meteorological analysis has been already performed with the “Centre D\'epartemental
de Loire-Atlantique de M\'et\'eo France”. 


\subsection{Distribution of rainfall intensity}

As a baseline approach, one may consider some statistics which encode some aspects of the Rainfall Intensity Distribution (RID) as proposed in \cite{Emmanuel2012162}:

\begin{compactitem}
\item percentage of very small values (thresholded according to the radar sensitivity)
\end{compactitem}
And with the remaining positive-valued intensities:
\begin{compactitem}
\item mean of the intensity's values
\item standard deviation 
\item median value\\
\end{compactitem}
%A mean of the results for each class has been made in order to get a
%first approach to how different are these kinds of rains.\\

Table \ref{Table:Rain statistics} shows the average values of those statistics for the four types of rainfall. Using those statistics which only encode the distribution of the intensity without any spatial information, we can see that the light rain has the lowest mean and zeros percentage. It means that light rain takes up
more area than other kind of rain, but with lower intensity. This  will make the light rain the easiest class to discriminate. On contrary, showers and unorganized storms will be the most difficult
classes to discriminate, as they have very similar statistics.


\begin{table*}[t]
\center
\begin{tabular}{ | c | c  c  c  c| }
\hline
& \bf \% Zeros & \bf Mean & \bf Std Dev & \bf Median\\
\hline
\bf Rain  & 28,7  (5,0) & 1,2 (0,5) & 1,1 (0,4) & 0,9 (0,4)\\
\hline 
\bf Shower & 79,7  (1,3) & 3,4 (6,0) & 7,6 (35,3) & 0,9 (0,2)\\
\hline
\bf O. storm & 56,1 (0,4) & 7,3 (16,9) &14,2 (77,2) & 2,5 (0,9) \\
 \hline
\bf U. storm & 67,8 (2,0) & 3,0 (4,2) & 6,3 (22,7) & 1,1 (0,4) \\
  \hline
\end{tabular}
\caption{Mean and variance of the RID statistics computed over the different rainfall types.}
\label{Table:Rain statistics}
\end{table*}

%\begin{table}[htbp]
%\raggedleft
%\begin{tabular}{ | c | c  c  c  c| }
%\hline
%& \bf \% Zeros & \bf Mean & \bf Std Dev & \bf Median\\
%\hline
%\bf Light rain  & 28,72  & 1,23 & 1,12 & 0,94\\
%\hline 
%\bf Shower & 79,73  & 3,35 & 7,59 & 0,91\\
%\hline
%\bf Unorg. storm & 67,82  & 2,95 & 6,33 & 1,09 \\
%  \hline
%\bf Org. storm & 56,10  & 7,32 & 14,23 & 2,45 \\
% \hline
%\end{tabular}
%\caption{Rain statistics for the 4 types of rainfall. \gerard{show the variance}}
%\label{Table:Rain statistics}
%\end{table}


In order to discriminate these two kind of rains, we have to consider other features that take into account the spatial configuration of the meteorological phenomenon. Section \ref{sec:scattering}
will explain the scattering transform, which will allow us to achieve a better classification
by providing this needed extra information.


\section{Scattering transform}
\label{sec:scattering}

To improve classification outcomes we represent the image using the scattering transform. An expected scattering 
representation of stationary processes is introduced for texture discrimination.
As opposed to the Fourier power spectrum, it gives information on higher order 
moments, and can thus discriminate non-Gaussian textures having the same 
power spectrum. 
A scattering transform computes a translation invariant representation
by cascading wavelet transforms and modulus pooling operators, which
average the amplitude of iterated wavelet coefficients. It is also Lipschitz
continuous to deformations, while preserving the signal energy \cite{CPA:CPA21413}.

 A scattering transform builds non-linear invariants from wavelet 
coefficient, with modulus and averaging pooling functions. 

Being $G$ a group of rotations $r$ of angles $2k\pi/K$ for $0 \leq  k < K$ 
two-dimensional directional wavelets are obtained by rotating a single band-pass filter
$\psi$ by $r \in G$ and dilating it by $2^j$ for $j \in Z$ (where j is the scale in the filter). 
More translation invariant coefficients can be obtained by further iterating on
 the wavelet transforms and modulus operators.
Let $U[\lambda]x = |x\star\psi_\lambda|$. Any sequence $p=(\lambda_1,\lambda_2  ,…,\lambda_m)$
 defines a path along which is computed an ordered product of non-linear and non-commuting operators \cite{6522407}.
$$U[p]x=U[\lambda_m]...U[\lambda_2]U[\lambda_1]x = |x\star\psi_1||x\star\psi_2|... |x\star\psi_m|$$

A Scattering transform along the path p is defined as an integral, normalized by the response of a Dirac:
$$S_x[p]=\mu^{-1}_p\int U[p]x(u)du \ \ with \ \  \mu_p=\int U[p]\delta(u)du$$	
Each Scattering coefficient is invariant to translation of $x$.
Considering classification tasks, it is often better to compute localized descriptors, which are
 invariant to translations, smaller than a predefined scale $2^J$, while keeping
 the spatial variability at scales larger than $2^j$. This is obtained by localizing 
the scattering integral with a scaled spatial window\cite{6522407}:
$$\phi_{2^J } (u)=2^{-2J}\phi(2^{-J} u)$$
It defines a windowed scattering transform in the neighborhood of u:
$$S[p]x(u)=U[p]x\star\phi_{2^J} (u)=\int U[p]x(v) \phi_{2^J} (u-v)dv$$
And hence:
$$S[p]x(u)=|x\star\psi_1 ||x\star\psi_2 |…|x\star\psi_m |·\phi_{2^J} (u)$$

$S[p]x(u)$ is a function of the window position u, which can be sub-sampled 
at intervals proportionate to the window size$2^J$.

\begin{figure}[t]
\centering
\centerline{\includegraphics[width=\linewidth]{Scatteringpropagator}}
\caption{A scattering propagator applied to $x$ computes the first layer of wavelet coefficient modulus $U[\lambda]x$ and outputs its local average $S[\phi]x=x\star\phi_{2^J}$. Applying the propagator to the first layer coefficients $U[\lambda_1]$ outputs first-order scattering coefficients $S[\lambda_1]x$ and computes the propagated signal on the second layer $U[\lambda_1,\lambda_2]$ }
\label{fig: Scattering convolution network}
\end{figure}


\begin{figure*}[t]
\centering
\centerline{\includegraphics[width=\linewidth]{lightrainstorm}}
\caption{Top figure: light rain. Bottom figure: organized storm. From left to right: radar image, power spectrum, first order scattering coefficients, and second order scattering coefficients.}
\label{fig:Scat_examples}
\end{figure*}


If $p=(\lambda_1,…\lambda_m)$ is a path of length $m$ then $S[p]x(u)$ is called a windowed 
scattering coefficient of order $m$. It is computed at the layer m of the specified convolution
network. For large scale invariants, several layers are
necessary to avoid losing crucial information.
For appropriate wavelets, first order coefficients $S[λ_1 ]x$ are equivalent to
scale-invariant feature transform (SIFT) coefficients as it computes the local sum of image gradient amplitudes
among image gradients having nearly the same direction in a histogram having
8 different direction bins \cite{sift}.
A Scattering transform computes higher-order coefficients
by further iterating on wavelet transforms and modulus operators. Wavelet coefficients
are computed up to a maximum scale $2^J$ and the lower frequencies are filtered by
$\phi_{2^J} (u)=2^{-2J}\phi(2^{-J} u)$. For a Morlet Wavelet the averaging filter
$ \varphi $ is chosen to be a Gaussian.



Figures \ref{fig:Scat_examples} show an example of how the scattering transform can discriminate very different rainfalls: light rain and organized storm. The display of the scattering coefficients is as follows: orientation changes along the azimuth and the scale along the radius. The display of the second order coefficients follows the same rationale, see \cite{CPA:CPA21413} for further details. Considering the power spectrum or the first order scattering coefficients does not allow an easy discrimination. On contrary, the second order scattering coefficients exhibit more energy for the organized storm. 

%Figure \ref{fig:showerthund} is an example of how the scattering transform can discriminate 
%two images from different classes which seems very similar and have a very similar power spectrum.

\section{Features design}
\label{sec:implantation}

The scattering transform is performed with 6 scales and 12 orientations for
a total of 2232 scattering coefficients (72 $1^{st}$ order coefficients and
2160 $2^{nd}$ order coefficients). $2^{nd}$ order scattering coefficients gather
the most important information for the classification. The numbers of scales and orientations are  those that give the best results on the considered database.

Figures \ref{fig:Scat_examples}   show that the orientation has
 a high influence in the scattering coefficients, especially the second order ones. This might indicate that 
the rain band has a particular shape which makes the scattering representation 
more powerful in a particular orientation. It is due  to the wind direction
 which will be orthogonal to the most powerful signal direction. For example, typical fronts are moving from the South-West to the North-East in western Europe. This information can be meaningful to discriminate this class. However, we believe that the features shall be invariant to changes of orientation.
Thus, the scattering coefficients are integrated 
over orientation as in \cite{DBLP:conf/cvpr/SifreM13}. So, there will be no more 
orientation components, but a unique coefficient defining the power for every
scale. Also, thanks to this  integration, the total number of coefficients is highly reduced:
from 2232 to 21 scattering coefficients (6 $1^{st}$ order coefficients and 15
$2^{nd}$ order coefficients).



%\begin{figure}[t]
%\centering
%\centerline{\includegraphics[width=\linewidth]{showerthunderstorm}}
%\caption{Top figure: shower. Bottom figure: unorganised storm. From left to right: radar image, power spectrum, first order scattering coefficients, and second order scattering coefficients.}
%\label{fig:showerthund}
%\end{figure}


\section{Experiments}
\label{sec:experiments}

In order to evaluate the merit of the different representations discussed, a leave-one-out classification scheme is taken using the k-nearest neighbors algorithm \cite{1053964} over a database of 1239 images, manually labeled in terms of rain type by  visual inspection. The cardinality of the rain type classes is as follows: 497 images of light rain, 399 images of showers, 294 images of unorganized storms, and 49 images of organized storms.

%Light rain is the most common kind of rain in the analyzed data with 497 of the 1239
%observations; showers constitute 399 observations, unorganised storms 294
%observations, and organised storms constitute only 49 of the observations.

%In the case study, for every image, the distance to all the others will be performed,
% and hence, a 1239X1239 matrix with a null diagonal will be created containing all the distances.
%Using these distances, the class of the new point will be determined by making the mode 
%of the k-nearest neighbours class.

Performance is measured using two metrics: the overall accuracy and the class wise accuracy. Overall accuracy is defined as the average number of correct predictions over the whole database with respect to the ground truth annotation. Class wise accuracy is the accuracy computed for each class and averaged over the classes. The latter is therefore independent of the distribution of the type of rainfall in the database.

%Prediction of the classifier is compared to a ground truth
%The accuracy of the evaluated approach is computed as follows : , as there has been a previous visual classification 
%of the images, it will be considered a right answer every time the class chosen by the algorithm 
%for an image coincides with the class predefined with the visual study. 
%The accuracy will be calculated by dividing the number of correct answers by all the amount of images.
%As the cardinality of the classes are very different, a second parameter, the accuracy per class is calculted by doing
%the mean of every class accuracy. 

\begin{table}[t]
\begin{tabular}[b]{  c | c   c}

& \bf Accuracy & \bf Class acc. \\
  \hline
\bf RID & 89,43 & 85,17 \\
 
\bf  Power Spectrum & 72,72 & 58,51\\

\bf $1^{st}$ order scattering & 90,88   & 88,93\\

\bf $2^{nd}$ order scattering & 93,22 & 90,85\\

\bf $1^{st} +  2^{nd}$ order scattering & 93,46 & 91,47\\
 
\end{tabular}
\caption{Overall performance.}
\label{Table: performance}
\end{table}

The results achieved by the different methods can be seen on Table \ref{Table: performance}. Remember that the 1$^{st}$ order scattering are conceptually equivalent to the SIFT, and can therefore be considered as a good baseline for texture classification. The RID features achieve good results with a drop in class wise accuracy. A closer look at the performance can be achieved by considering Table \ref{Table:confusionmatrid}. For the largest class (Light rain), the performance is quite good, whereas organised storms are not well modeled, which affects the class wise accuracy. As storms exhibit spatial structure, one can expect that a frequency decomposition of the image might lead to better performance. This is not the case of the power spectrum, probably due to a lack of stability. The first order scattering performs better, while the second is considerably better. Considering the difference between the confusion matrices of the 1$^st$ and 2$^nd$ orders shown on respectively Tables \ref{Table:confusionmat1st} and \ref{Table:confusionmat1st2nd}, we see that the classes which require a finer modeling of the spatial structure are better handled by the latter. This is  probably due to the fact that higher order statistics are nicely encoded by the 2$^nd$ order scattering coefficients.

Further, applying a Principal Components Analysis (PCA) over the
 scattering transform integrated over rotation, (both 1$^st$ and 2$^nd$ scattering coefficients) leads to 11 principal components which further improve performance. The combination of those features with the RID statistics further achieve a satisfactory score of almost 98 \% in accuracy and class wise accuracy, which tends to demonstrate the complementarity of distribution based features, and spatial  based features.
 
 
%The images have been also integrated by 6, obtaining \mathieu{providing ?} the representation of half an hour.
%By integrating, scattering transform gets more stability and a representation of the weather
%fluctuation is gotten.



\begin{table}[t]
\begin{tabular}[b]{  c | c  c  c  c }

 & \bf Rain & \bf Shower & \bf U. Storm &  \bf O. Storm\\
  \hline
\bf Rain & \textbf{98,2} & 0,2 & 1,6 & 0,0 \\
 
\bf  Shower & 0,0 & \textbf{89,7} & 10,0 & 0,3 \\

\bf U. Storm& 1,7 & 17,4 & \textbf{78,9} & 2,0 \\

\bf O. Storm & 0,0 & 6,1 & 26,5 & \textbf{67,4} \\

\end{tabular}
\caption{Confusion matrix of the RID statistics.}
\label{Table:confusionmatrid}
\end{table}

%Main problems are caused by the similarity of the showers and the unorganised storms.
% This similarity results in a 17,35\% of the unorganised storms misclassified
% as showers and 10\% of the showers misclassified as unorganised storms.
%There is also the exceptional case of the organised storms which has a 
%very low accuracy due to the lack of observations of its class.


\begin{table}[t]
\begin{tabular}[b]{  c | c  c  c  c }

 & \bf Rain & \bf Shower & \bf U. Storm &  \bf O. Storm\\
  \hline
\bf Rain & \textbf{97,6} & 1,2 & 1,2 & 0,0\\

\bf  Shower & 3,3 & \textbf{87,7} & 7,5 & 1,5\\

\bf U. Storm & 1,7 & 13,6 & \textbf{84,7} & 0 \\

\bf O. Storm & 0,0 & 8,2 & 6,1 & \textbf{85,7} \\

\end{tabular}
%\begin{center} Accuracy = 93,22 ; Accuracy per class = 90,85 \end{center}
\caption{Confusion matrix of the $1^{st}$ order scattering coefficients.}
\label{Table:confusionmat1st}
\end{table}

%There is a highly improvement in all the classes by using the scattering transform 
%instead of using the statistics. Table \ref{Table:confusionmat1st2nd} describes the results
%merging the statistics and the scattering coefficients. It has to be taken into account
%that there are only 4 statistics for more than 2000 scattering coefficients. This is the
%reason why the statistics are more weighted in order to give to all the coefficients a 
%similar importance.

\begin{table}[t]
\begin{tabular}[b]{  c | c  c  c  c }

 & \bf Rain & \bf Shower & \bf U. Storm &  \bf O. Storm\\
  \hline
\bf Light rain & \textbf{99,0} & 0,6 & 0,4 & 0,0\\

\bf  Shower & 1,2 & \textbf{91,7} & 6,3 & 0,8 \\

\bf U. Storm & 1,4 & 11,2 & \textbf{87,4} & 0,3 \\

\bf O. Storm & 0,0 & 2,0 & 10,2 & \textbf{87,8} \\

\end{tabular}
%\begin{center} Accuracy = 97,18 ; Accuracy per class = 96,22 \end{center}
\caption{Confusion matrix of the $2^{nd}$ order scattering coefficients.} % GERARD provide correct data
\label{Table:confusionmat1st2nd}
\end{table}

%The result is much better than using only the statistics or the scattering coefficients.
%This is the proof of the complementarity of these two kind of parameters. The showers
%and the unorganised storms are no longer highly confused, less than 4\% in both cases.
%
%
%
%These are the results of using just one kind of coefficients.\mathieu{Concerning the use of one kind of coefficients, we} We can see that
%the scattering transform gets an improvement \mathieu{shows an improvement when compared} comparing with the statistics
%and the power spectrum which, in this case, does not get a really good 
%performance.
%Table \ref{Table:Performance Enhanced} shows how merging the statistics and the scattering
%transform (also with just a PCA of the scattering transform) turn into an 
%even better result.

\begin{table}[t]
\begin{tabular}[b]{  c | c   c }

& \bf Accuracy & \bf Class accuracy \\
  \hline
\bf PCA Scattering  & 93,87 & 92,24  \\

\bf RID + Scattering & 97,18 & 96,22  \\

\bf RID + PCA Scat & 98,22 & 97,95  \\

\end{tabular}
\caption{Performance of enhanced feature sets.}
\label{Table:Performance Enhanced}
\end{table}


\section{Conclusion}

Satisfactory results reported in this paper show the relevance of the use of the scattering transform to predict the rainfall type from rainfall intensity radar measurements.
of the scattering transform for classifying the radar images used in rainfall prediction.
 It is expected to be relevant when classifying new radar images. It has also been demonstrated  that the RID statistics of the images and the 
scattering transform have complementary information which, if jointly considered, achieve a
 high classification score when merging both information on the studied database. Future work will consist in considering more principled classification schemes such as the Support Vector Machines (SVM)s over a larger scale database and comparing with other state of the art local features commonly used for texture classification \cite{1498756}.



% References should be produced using the bibtex program from suitable
% BiBTeX files (here: strings, refs, manuals). The IEEEbib.bst bibliography
% style file from IEEE produces unsorted bibliography list.
% -------------------------------------------------------------------------
\bibliographystyle{IEEEbib}
\bibliography{biblio}



\end{document}
